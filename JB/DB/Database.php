<?php

namespace JB\DB;

/**
 * Class to manage the database connection
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Connection
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class Database
{
	/**
	 * PDO-Instance
	 * @access private
	 */
	private $pdo;

	/**
	 * Database dialect
	 * @access private
	 */
	private $dialect;

	/**
	 * SQL generator instance
	 * @access private
	 */
	private $sqlGen;

	/**
	 * Constructor with PDO-instance
	 * @access private
	 * @param PDO    $pdo     PDO-instance
	 * @param string $dialect Database dialect
	 */
	private function __construct(\PDO $pdo, $dialect)
	{
		$this->pdo = $pdo;
		$this->dialect = $dialect;
		$this->sqlGen = SQLGenerator::Get($dialect);
	}

	/**
	 * Inserts an entry in the given table
	 * @access public
	 * @param string $table Table name
	 * @param array  $set   List of columns to set
	 * @return \JB\DB\Writable The inserted object
	 * @todo throw error if insert fails
	 */
	public function Insert($table, array $set)
	{
		$key = $this->RawNewKey($table, $set);
		if (is_null($key))
		{
			return null;
		}
		else
		{
			$mod = new Writable($this, $table);
			$mod->UseId($key);
			return $mod;
		}
	}

	/**
	 * Selects one object by the primary key
	 * @access public
	 * @param string $table Table name
	 * @param scalar $id    Primary key value
	 * @return \JB\DB\Writable Requested object
	 */
	public function SingleFrom($table, $id)
	{
		$mod = new Writable($this, $table);
		$mod->UseId($id);
		return $mod;
	}

	/**
	 * Selects an object list from the given table and by the given condition
	 * @access public
	 * @param string                 $table Table name
	 * @param \JB\DB\OP\Where\IWhere $op    Condition
	 * @return \JB\DB\Selection Requested object collection
	 */
	public function From($table, OP\Where\IWhere $op)
	{
		return Selectable::From($this, $table)->Where($op);
	}

	/**
	 * Selects an object list from the given table
	 * @access public
	 * @param string $table Table name
	 * @return \JB\DB\Selection Requested object collection
	 */
	public function FromAll($table)
	{
		return Selectable::From($this, $table);
	}

	/**
	 * Executes a PDO-Statement and returns it
	 * @param string       $query Query to execute
	 * @param \JB\DB\Binds $binds Values to bind
	 * @return PDOStatement PDOStatement instance
	 * @todo improve error handling
	 */
	public function Statement($query, Binds $binds)
	{
		$stmt = $this->pdo->prepare($query);
		if ($stmt !== false && $stmt->execute($binds->AsArray()))
		{
			return $stmt;
		}
		elseif ($stmt !== false)
		{
			var_dump($stmt->errorInfo(), $query, $binds);
			echo PHP_EOL;
		}
		else
		{
			var_dump($this->pdo->errorInfo(), $query, $binds);
			echo PHP_EOL;
		}

		return null;
	}

	/**
	 * Executes the given query and returns the result as array
	 * @access public
	 * @param array                       $select The columns to select
	 * @param string                      $table  The table or view name
	 * @param \JB\DB\OP\Where\IWhere|null $where  The condition
	 * @param array|null                  $order  The columns to order by
	 * @param int                         $skip   The amount of items to skip
	 * @param int                         $take   The amount to return (-1 = all)
	 * @return array|null Requested data or null if an error occured
	 * @todo throw error if table name empty
	 */
	public function RawRequest(array $select, $table, $where = null, $order = null, $skip = 0, $take = -1)
	{
		list($command, $bind) = $this->sqlGen->Select($select, $table, $where, $order, $skip, $take);
		$stmt = $this->Statement($command, $bind);
		return is_null($stmt) ? null : $stmt->fetchAll();
	}

	/**
	 * Executes the given query and returns the last created primary key
	 * @access public
	 * @param string $table The table name
	 * @param array  $set   The values to set
	 * @return scalar Any created primary key
	 */
	public function RawNewKey($table, $set = null)
	{
		list($command, $bind) = $this->sqlGen->Insert($table, is_null($set) ? array() : $set);
		$stmt = $this->Statement($command, $bind);
		return is_null($stmt) ? null : $this->pdo->lastInsertId();
	}

	/**
	 * Executes the given query and returns number of changed rows
	 * @access public
	 * @param string                      $table The table name
	 * @param array                       $set   The set operations
	 * @param \JB\DB\OP\Where\IWhere|null $where The condition
	 * @return int Count of changed rows
	 * @todo throw error if table name empty
	 */
	public function RawRowCount($table, array $set, $where)
	{
		list($command, $bind) = $this->sqlGen->Update($table, $set, $where);
		$stmt = $this->Statement($command, $bind);
		return is_null($stmt) ? -1 : $stmt->rowCount();
	}

	/**
	 * All table definitions
	 * @access private
	 */
	private $definitions = array();

	/**
	 * Returns the definition of the given table
	 * @param string $table The table
	 * @return array The definition
	 */
	public function GetDefinition($table)
	{
		if (!array_key_exists($table, $this->definitions))
		{
			$this->definitions[$table] = $this->sqlGen->Definition($this, $table);
		}

		return $this->definitions[$table];
	}

	/**
	 * Tries to connect to a database file
	 * @access public
	 * @param string $file    Path to file
	 * @param string $driver  Driver to perform on the database file
	 * @param string $dialect The database dialect from the database file
	 * @return \JB\DB\Database A connected instance
	 * @static
	 * @todo make a real try not just return null
	 */
	public static function TryConnectToFile($file, $driver, $dialect)
	{
		if (self::connectByConnectionString("$file", null, null, $driver, $pdo))
		{
			return new self($pdo, $dialect);
		}

		return null;
	}

	/**
	 * Tries to connect to a database server
	 * @access public
	 * @param string $dns      Database DNS
	 * @param string $user     Username
	 * @param string $password Password
	 * @param string $schema   Database schema
	 * @param string $driver   Driver to perform on the database server
	 * @param string $dialect  The database dialect from the database server
	 * @return \JB\DB\Database A connected instance
	 * @static
	 * @todo make a real try not just return null
	 */
	public static function TryConnectToServer($dns, $user, $password, $schema, $driver, $dialect)
	{
		if (self::connectByConnectionString("host=$dns;dbname=$schema", $user, $password, $driver, $pdo))
		{
			return new self($pdo, $dialect);
		}

		return null;
	}

	/**
	 * Connects a PDO-instance by a connection string
	 * @access private
	 * @param string $connectionString The connection string to use
	 * @param string $user             Username
	 * @param string $password         Password
	 * @param string $driver           Driver to perform on the database
	 * @param PDO    $out              The PDO instance
	 * @return bool True, if the PDO instance is connected, otherwise false
	 * @static
	 * @todo improve error handling
	 */
	private static function connectByConnectionString($connectionString, $user, $password, $driver, &$out)
	{
		try
		{
			$pdo = new \PDO("$driver:$connectionString", $user, $password);
			$out = $pdo;
			return true;
		}
		catch (\PDOException $e)
		{
			echo "{$e->getCode()}: {$e->getMessage()} (".print_r($e->errorInfo, true).
					") at line {$e->getLine()} in file: {$e->getFile()}";
		}

		return false;
	}
};