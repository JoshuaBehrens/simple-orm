<?php

namespace JB\DB;

require_once(dirname(__FILE__)."/Helper/MySQL.php");
require_once(dirname(__FILE__)."/Helper/SQLite.php");

abstract class SQLGenerator
{
	/**
	 * Gets a specific SQLGenerator from the given dialect
	 * @param string $dialect The dialect
	 * @return \JB\DB\SQLGenerator A generator object if one is registered for the given dialect otherwise null
	 * @todo throw error
	 */
	public static function Get($dialect)
	{
		switch (strtolower($dialect))
		{
			case 'mysql':
				return new SQL\MySQL();
			case 'sqlite':
				return new SQL\SQLite();
		}

		return null;
	}

	/**
	 * Generates from the given values a select statement and the used late-binds
	 * @abstract
	 * @access public
	 * @param array                       $select The columns to select
	 * @param string                      $from   The table or view name
	 * @param \JB\DB\OP\Where\IWhere|null $where  The where condition or null if no condition given
	 * @param array|null                  $order  The order to sort
	 * @param int                         $skip   The amount of items to skip
	 * @param int                         $take   The amount to return
	 * @return array Array containing the sql statement and the late-binds
	 */
	public abstract function Select(array $select, $from, $where, $order, $skip, $take);

	/**
	 * Generates from the given values an update statement and the used late-binds
	 * @abstract
	 * @access public
	 * @param string                      $table The table
	 * @param array                       $set   The columns to set
	 * @param \JB\DB\OP\Where\IWhere|null $where The where condition or null if no condition given
	 * @return array Array containing the sql statement and the late-binds
	 */
	public abstract function Update($table, array $set, $where);

	/**
	 * Generates from the given values an insert statement and the used late-binds
	 * @abstract
	 * @access public
	 * @param string $table  The table
	 * @param array  $values The values to insert
	 * @return array Array containing the sql statement and the late-binds
	 */
	public abstract function Insert($table, array $values);

	/**
	 * Gets a table definition from the given database and table/view
	 * @abstract
	 * @access public
	 * @param \JB\DB\Database $database The database connection
	 * @param string          $table    The table/view name
	 * @return \JB\DB\TableDefinition The table definition
	 */
	public abstract function Definition(Database $database, $table);
};
