<?php

require_once(dirname(__FILE__)."/../All.php");

use JB\DB\Database;
use JB\DB\Binds;
use JB\DB\Selectable;
use JB\DB\OP\Where;

$filename = basename(__FILE__, ".php").".db";

if (!is_null($con = Database::TryConnectToFile($filename, "sqlite", "sqlite")))
{
	$con->Statement("CREATE TABLE test(uniq INTEGER PRIMARY KEY, name TEXT, wert TEXT);", new Binds());
	$con->Statement("INSERT INTO test(name, wert) VALUES('1', 'wert #1');", new Binds());
	$con->Statement("INSERT INTO test(name, wert) VALUES('2', 'wert #2');", new Binds());

	$list = Selectable::From($con, 'test');
	foreach ($list->Iter() as $item)
	{
		echo $item->Get('wert')."\t= ".$item->Get('wert').PHP_EOL;
	}
	foreach ($list->Where(Where\Eq('name', 2))->Iter() as $item)
	{
		echo $item->Get('wert')."\t= ".$item->Get('wert').PHP_EOL;
	}
	foreach ($list->Where(Where\Neq('name', 2))->Iter() as $item)
	{
		echo $item->Get('wert')."\t= ".$item->Get('wert').PHP_EOL;
	}
	foreach ($list->Where(Where\Gt('name', 1))->Iter() as $item)
	{
		echo $item->Get('wert')."\t= ".$item->Get('wert').PHP_EOL;
	}
	foreach ($list->Where(Where\Lt('name', 2))->Iter() as $item)
	{
		echo $item->Get('wert')."\t= ".$item->Get('wert').PHP_EOL;
	}
	foreach ($list->Where(Where\Like('wert', '%1'))->Iter() as $item)
	{
		echo $item->Get('wert')."\t= ".$item->Get('wert').PHP_EOL;
	}
}

if (file_exists($filename))
{
	unlink($filename);
}
