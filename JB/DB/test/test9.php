<?php

require_once(dirname(__FILE__)."/../All.php");

use JB\DB\Database;
use JB\DB\Binds;
use JB\DB\Selectable;
use JB\DB\OP\Set;

$filename = basename(__FILE__, ".php").".db";

if (!is_null($con = Database::TryConnectToFile($filename, "sqlite", "sqlite")))
{
	$con->Statement("CREATE TABLE test(uniq INTEGER PRIMARY KEY, name INTEGER, wert TEXT);", new Binds());
	echo $con->Insert("test", array(Set\Value('name', 1), Set\Value('wert', 'wert #1')))
					 ->Format("Insert {name}\t= {wert}\n");
	echo $con->Insert("test", array(Set\Value('name', 2), Set\Value('wert', 'wert #2')))
					 ->Format("Insert {name}\t= {wert}\n");
	echo $con->Insert("test", array(Set\Value('name', 3), Set\Value('wert', 'wert #3')))
					 ->Format("Insert {name}\t= {wert}\n");
	echo $con->Insert("test", array(Set\Value('name', 4), Set\Value('wert', 'wert #4')))
					 ->Format("Insert {name}\t= {wert}\n");

	foreach (Selectable::From($con, 'test')->Take(2)
																				 ->Iter() as $row)
	{
		echo $row->Format("PLS: {uniq}\t= {wert}\n");
	}

	foreach (Selectable::From($con, 'test')->Skip(2)
																				 ->Iter() as $row)
	{
		echo $row->Format("PLS: {uniq}\t= {wert}\n");
	}

	foreach (Selectable::From($con, 'test')->Take(2)
																				 ->Skip(2)
																				 ->Iter() as $row)
	{
		echo $row->Format("PLS: {uniq}\t= {wert}\n");
	}

	echo Selectable::From($con, 'test')->Page(1, 2)->Format("PLS: {uniq}\t= {wert}\n");

	echo "If the pagesize is 3 and I look at the first page I will get ".
				Selectable::From($con, 'test')->Page(1, 3)->Count()." items\n";
}

if (file_exists($filename))
{
	unlink($filename);
}
