<?php

require_once(dirname(__FILE__)."/../All.php");

use JB\DB\Database;
use JB\DB\Binds;
use JB\DB\OP\Set;
use JB\DB\OP\Order;
use JB\DB\OP\Where;
use JB\DB\Selectable;
use JB\DB\Updatable;

$filename = basename(__FILE__, ".php").".db";

if (!is_null($con = Database::TryConnectToFile($filename, "sqlite", "sqlite")))
{
	$con->Statement("CREATE TABLE test(uniq INTEGER PRIMARY KEY, name INTEGER, wert TEXT);", new Binds());
	echo $con->Insert("test", array(Set\Value('name', 1), Set\Value('wert', 'wert #1')))
					 ->Format("Insert {name}\t= {wert}\n");
	echo $con->Insert("test", array(Set\Value('name', 2), Set\Value('wert', 'wert #2')))
					 ->Format("Insert {name}\t= {wert}\n");
	echo $con->Insert("test", array(Set\Value('name', 3), Set\Value('wert', 'wert #3')))
					 ->Format("Insert {name}\t= {wert}\n");
	echo $con->Insert("test", array(Set\Value('name', 4), Set\Value('wert', 'wert #4')))
					 ->Format("Insert {name}\t= {wert}\n");

	foreach (Selectable::From($con, 'test')->Select(array('*'))
																				 ->Where(Where\Gt('name', 1))
																				 ->OrderBy(Order\Desc('uniq'))
																				 ->Iter() as $row)
	{
		echo $row->Format("PLS: {uniq}\t= {wert}\n");
	}

	$plannedUpdate = Updatable::On($con, 'test')->AndSet(Set\Value('name', -1))->Where(Where\Eq('name', 3));
	$possible = $plannedUpdate->Count();
	$changes = $plannedUpdate->Perform();
	echo "Updated $changes of $possible possible row(s)\n";

	foreach (Selectable::From($con, 'test')->Select(array('*'))
																				 ->Where(Where\Gt('name', 1))
																				 ->OrderBy(Order\Desc('uniq'))
																				 ->Iter() as $row)
	{
		echo $row->Format("PLS: {uniq}\t= {wert}\n");
	}
}

if (file_exists($filename))
{
	unlink($filename);
}
