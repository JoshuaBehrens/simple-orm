<?php

require_once(dirname(__FILE__)."/../All.php");

use JB\DB\Database;
use JB\DB\Binds;
use JB\DB\Selectable;
use JB\DB\OP\Set;
use JB\DB\OP\Where;

$filename = basename(__FILE__, ".php").".db";

if (!is_null($con = Database::TryConnectToFile($filename, "sqlite", "sqlite")))
{
	$con->Statement("CREATE TABLE test(uniq INTEGER PRIMARY KEY, name TEXT, wert TEXT);", new Binds());
	echo $con->Insert("test", array(Set\Value('name', '1'), Set\Value('wert', 'wert #1')))
					 ->Format("Insert {name}\t= {wert}\n");
	echo $con->Insert("test", array(Set\Value('name', '2'), Set\Value('wert', 'wert #2')))
					 ->Format("Insert {name}\t= {wert}\n");
	echo $con->Insert("test", array(Set\Value('name', '3'), Set\Value('wert', 'wert #3')))
					 ->Format("Insert {name}\t= {wert}\n");
	echo $con->Insert("test", array(Set\Value('name', '4'), Set\Value('wert', 'wert #4')))
					 ->Format("Insert {name}\t= {wert}\n");

	$list = $con->From('test', Where\Gt('name', 2));

	foreach ($list->Iter() as $item)
	{
		echo $item->Format("\t{name}\t= {wert}\n");
	}
	foreach ($list->AndWhere(Where\Unlike('wert', '%3'))->Iter() as $item)
	{
		echo $item->Format("\t{name}\t= {wert}\n");
	}
}

if (file_exists($filename))
{
	unlink($filename);
}
