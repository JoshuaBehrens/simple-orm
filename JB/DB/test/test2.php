<?php

require_once(dirname(__FILE__)."/../All.php");

use JB\DB\Database;
use JB\DB\Binds;
use JB\DB\Selectable;
use JB\DB\Writable;

$filename = basename(__FILE__, ".php").".db";

if (!is_null($con = Database::TryConnectToFile($filename, "sqlite", "sqlite")))
{
	$con->Statement("CREATE TABLE test(uniq INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, wert TEXT);", new Binds());
	$con->Statement("INSERT INTO test(name, wert) VALUES('1', 'wert #1');", new Binds());
	$con->Statement("INSERT INTO test(name, wert) VALUES('2', 'wert #2');", new Binds());

	$list = Selectable::From($con, 'test');
	foreach ($list->Iter() as $item)
	{
		echo $item->Format("{name}\t= {wert}\n");
	}

	$obj = new Writable($con, "test");
	$obj->UseId(2);
	print_r(array($obj->Exists() ? "true" : "false", $obj->Get("name"), $obj->Get("wert")));
	$obj->Set("name", "Der wahre Name", false);
	print_r(array($obj->Exists() ? "true" : "false", $obj->Get("name"), $obj->Get("wert")));
}

if (file_exists($filename))
{
	unlink($filename);
}
