<?php

namespace JB\DB;

require_once(dirname(__FILE__)."/Object.php");
require_once(dirname(__FILE__)."/IGetable.php");

/**
 * Class to hold readable data of a table or view row
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Connection
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class Readable extends Object implements IGetable
{
	protected $table;
	protected $data;
	private $id;

	/**
	 * Constructor of the readable initializing the data and database connection
	 * @access public
	 * @param \JB\DB\Database $database The database connection
	 * @param string          $table    The table or view name
	 * @param array           $data     The data to hold
	 */
	public function __construct(Database $database, $table, array $data)
	{
		parent::__construct($database);
		$this->table = $database->GetDefinition($table);
		$this->data = $data;
	  $this->id = null;
	}

	/**
	 * Gets the value lying behind the column name
	 * @access public
	 * @param string $col The colum name
	 * @return scalar|null The value of the column
	 */
	public function Get($col)
	{
		if (array_key_exists($col, $this->data))
		{
			return $this->data[$col];
		}

		return null;
	}

	/**
	 * Formats the data into a string by the given pattern
	 * @access public
	 * @param string $format The format string
	 * @return string The formatted data
	 */
	public function Format($format)
	{
		$data = $this->data;
		return preg_replace_callback('#\{(?<name>[^}]+)\}#', function ($matches) use ($data)
			{
				if (array_key_exists($matches['name'], $data))
				{
					return $data[$matches['name']];
				}

				return $matches[0];
			}, $format);
		return $format;
	}
};