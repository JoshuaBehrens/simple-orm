<?php

namespace JB\DB;

require_once(dirname(__FILE__)."/Database.php");

class Updatable extends Object
{
	/**
	 * Containing the where clause
	 * @access private
	 */
	private $where;

	/**
	 * Containing the set clause
	 * @access private
	 */
	private $set;

	/**
	 * The table name
	 * @access private
	 */
	private $table;

	/**
	 * Adds a set to the list
	 * @access private
	 * @param \JB\DB\OP\Set\ISet $op The set operation to add
	 * @return void
	 */
	private function setAdd(OP\Set\ISet $op)
	{
		if (is_null($this->set))
		{
			$this->set = array();
		}

		$this->set[] = $op;
	}

	/**
	 * Adds a where object to the list combining with an and operator
	 * @access private
	 * @param \JB\DB\OP\Where\IWhere $op The where operator
	 * @return void
	 */
	private function whereAnd(OP\Where\IWhere $op)
	{
		if (is_null($this->where))
		{
			$this->where = $op;
		}
		else
		{
			OP\Where\LogicalAnd($this->where, $op);
		}
	}

	/**
	 * Adds a where object to the list combining with an or operator
	 * @access private
	 * @param \JB\DB\OP\Where\IWhere $op The where operator
	 * @return void
	 */
	private function whereOr(OP\Where\IWhere $op)
	{
		if (is_null($this->where))
		{
			$this->where = $op;
		}
		else
		{
			OP\Where\LogicalOr($this->where, $op);
		}
	}

	/**
	 * The constructor taking the connection and the table to work on
	 * @access protected
	 * @param \JB\DB\Database $database The database connection
	 * @param string          $table    The table
	 * @todo throw error if view
	 */
	protected function __construct(Database $database, $table)
	{
		parent::__construct($database);

		$this->where = null;
		$this->set = null;
		$this->order = null;

		$this->table = $database->GetDefinition($table);
	}

	/**
	 * A factory creating a new updatable
	 * @access public
	 * @param \JB\DB\Database $database The database connection
	 * @param string          $table    The table or view name
	 * @static
	 * @return \JB\DB\Updatable The new updatable instance
	 */
	public static function On(Database $database, $table)
	{
		return new self($database, $table);
	}

	/**
	 * Sets all columns to select
	 * @access public
	 * @param array $ops The set operations
	 * @return \JB\DB\Updatable The new updatable instance
	 * @todo throw error
	 */
	public function Set(array $ops)
	{
		if (empty($this->set))
		{
			$result = clone $this;
			foreach ($ops as $op)
			{
				$result->setAdd($op);
			}

			return $result;
		}
	}

	/**
	 * Add a column to select
	 * @access public
	 * @param \JB\DB\OP\Set\ISet $op The operation to add
	 * @return \JB\DB\Updatable The new updatable instance
	 */
	public function AndSet(OP\Set\ISet $op)
	{
		$result = clone $this;
		$result->setAdd($op);
		return $result;
	}

	/**
	 * Sets the condition
	 * @access public
	 * @param \JB\DB\OP\Where\IWhere $op The condition
	 * @return \JB\DB\Updatable The new updatable instance
	 */
	public function Where(OP\Where\IWhere $op)
	{
		$result = clone $this;
		$result->where = $op;
		return $result;
	}

	/**
	 * Combining the existing condition with the new one (and operator)
	 * @access public
	 * @param \JB\DB\OP\Where\IWhere $op The condition
	 * @return \JB\DB\Updatable The new updatable instance
	 */
	public function AndWhere(OP\Where\IWhere $op)
	{
		$result = clone $this;
		$result->whereAnd($op);
		return $result;
	}

	/**
	 * Combining the existing condition with the new one (or operator)
	 * @access public
	 * @param \JB\DB\OP\Where\IWhere $op The condition
	 * @return \JB\DB\Updatable The new updatable instance
	 */
	public function OrWhere(OP\Where\IWhere $op)
	{
		$result = clone $this;
		$result->whereOr($op);
		return $result;
	}

	/**
	 * Counts the items of the given selection that should get changed
	 * @access public
	 * @return int The number of items
	 */
	public function Count()
	{
		$data = $this->getConnection()->RawRequest(array('1'),
																							 $this->table->GetName(),
																							 $this->where);
		return count($data);
	}

	/**
	 * Executes the update
	 * @access public
	 * @return int The number of rows that got updated
	 * @todo throw error if no set is given
	 */
	public function Perform()
	{
		return $this->getConnection()->RawRowCount($this->table->GetName(), $this->set, $this->where);
	}
};