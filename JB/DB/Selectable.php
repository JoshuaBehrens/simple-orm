<?php

namespace JB\DB;

require_once(dirname(__FILE__)."/Database.php");

class Selectable extends Object
{
	/**
	 * Containing the where clause
	 * @access private
	 */
	private $where;

	/**
	 * Containing the select clause
	 * @access private
	 */
	private $select;

	/**
	 * Containing the order clause
	 * @access private
	 */
	private $order;

	/**
	 * The amount of items to skip
	 * @access private
	 */
	private $skip;

	/**
	 * The amount of items to take
	 * @access private
	 */
	private $take;

	/**
	 * The table name
	 * @access private
	 */
	private $table;

	/**
	 * Adds a select to the list
	 * @access private
	 * @param string $col The column name to add
	 * @return void
	 * @todo throw error
	 */
	private function selectAdd($col)
	{
		if (is_string($col))
		{
			if (is_null($this->select))
			{
				$this->select = array();
			}

			$this->select[] = $col;
		}
	}

	/**
	 * Adds an order object to the list
	 * @access private
	 * @param \JB\DB\OP\Order\IOrder $op The order operator
	 * @return void
	 */
	private function orderAdd(OP\Order\IOrder $op)
	{
		if (is_null($this->order))
		{
			$this->order = array();
		}

		$this->order[] = $op;
	}

	/**
	 * Adds a where object to the list combining with an and operator
	 * @access private
	 * @param \JB\DB\OP\Where\IWhere $op The where operator
	 * @return void
	 */
	private function whereAnd(OP\Where\IWhere $op)
	{
		if (is_null($this->where))
		{
			$this->where = $op;
		}
		else
		{
			$this->where = OP\Where\LogicalAnd($this->where, $op);
		}
	}

	/**
	 * Adds a where object to the list combining with an or operator
	 * @access private
	 * @param \JB\DB\OP\Where\IWhere $op The where operator
	 * @return void
	 */
	private function whereOr(OP\Where\IWhere $op)
	{
		if (is_null($this->where))
		{
			$this->where = $op;
		}
		else
		{
			$this->where = OP\Where\LogicalOr($this->where, $op);
		}
	}

	/**
	 * The constructor taking the connection and the table to work on
	 * @access protected
	 * @param \JB\DB\Database $database The database connection
	 * @param string          $table    The table or view name
	 */
	protected function __construct(Database $database, $table)
	{
		parent::__construct($database);

		$this->where = null;
		$this->select = null;
		$this->order = null;
		$this->take = -1;
		$this->skip = 0;

		$this->table = $database->GetDefinition($table);
	}

	/**
	 * A factory creating a new selectable
	 * @access public
	 * @param \JB\DB\Database $database The database connection
	 * @param string          $table    The table or view name
	 * @static
	 * @return \JB\DB\Selectable The new selectable instance
	 */
	public static function From(Database $database, $table)
	{
		return new self($database, $table);
	}

	/**
	 * Sets all columns to select
	 * @access public
	 * @param array $cols The column names to select
	 * @return \JB\DB\Selectable A new selectable instance
	 * @todo throw error
	 */
	public function Select(array $cols)
	{
		if (empty($this->select))
		{
			$result = clone $this;
			foreach ($cols as $col)
			{
				$result->selectAdd($col);
			}

			return $result;
		}
	}

	/**
	 * Add a column to select
	 * @access public
	 * @param string $col The column name to add
	 * @return \JB\DB\Selectable A new selectable instance
	 */
	public function AndSelect($col)
	{
		$result = clone $this;
		$result->selectAdd($col);
		return $result;
	}

	/**
	 * Sets the rule to order by
	 * @access public
	 * @param \JB\DB\OP\Order\IOrder $op The order rule to set
	 * @return \JB\DB\Selectable A new selectable instance
	 * @todo throw error
	 */
	public function OrderBy(OP\Order\IOrder $op)
	{
		if (empty($this->order))
		{
			$result = clone $this;
			$result->orderAdd($op);
			return $result;
		}
	}

	/**
	 * Adds the rule to order by
	 * @access public
	 * @param \JB\DB\OP\Order\IOrder $op The order rule to add
	 * @return \JB\DB\Selectable A new selectable instance
	 */
	public function ThenBy($op)
	{
		$result = clone $this;
		$result->orderAdd($op);
		return $result;
	}

	/**
	 * Sets the condition
	 * @access public
	 * @param \JB\DB\OP\Where\IWhere $op The condition
	 * @return \JB\DB\Selectable A new selectable instance
	 */
	public function Where(OP\Where\IWhere $op)
	{
		$result = clone $this;
		$result->where = $op;
		return $result;
	}

	/**
	 * Combining the existing condition with the new one (and operator)
	 * @access public
	 * @param \JB\DB\OP\Where\IWhere $op The condition
	 * @return \JB\DB\Selectable A new selectable instance
	 */
	public function AndWhere(OP\Where\IWhere $op)
	{
		$result = clone $this;
		$result->whereAnd($op);
		return $result;
	}

	/**
	 * Combining the existing condition with the new one (or operator)
	 * @access public
	 * @param \JB\DB\OP\Where\IWhere $op The condition
	 * @return \JB\DB\Selectable A new selectable instance
	 */
	public function OrWhere(OP\Where\IWhere $op)
	{
		$result = clone $this;
		$result->whereOr($op);
		return $result;
	}

	/**
	 * Limit the selection on the given amount on items to take
	 * @access public
	 * @param int $amount The amount of items to take
	 * @return \JB\DB\Selectable A new selectable instance
	 */
	public function Take($amount)
	{
		$result = clone $this;
		$result->take = $amount;
		return $result;
	}

	/**
	 * Limit the selection ignoring the first given amount on items
	 * @access public
	 * @param int $amount The amount of items to ignore
	 * @return \JB\DB\Selectable A new selectable instance
	 */
	public function Skip($amount)
	{
		$result = clone $this;
		$result->skip = $amount;
		return $result;
	}

	/**
	 * Limit the selection on the given page
	 * @access public
	 * @param int $page     The page number
	 * @param int $pagesize The page size
	 * @return \JB\DB\Selectable A new selectable instance
	 */
	public function Page($page, $pagesize)
	{
		$result = clone $this;
		$result->skip = $page * $pagesize;
		$result->take = $pagesize;
		return $result;
	}

	/**
	 * Counts the items of the given selection
	 * @access public
	 * @return int The number of items
	 */
	public function Count()
	{
		$data = $this->getConnection()->RawRequest(array('1'),
																							 $this->table->GetName(),
																							 $this->where,
																							 $this->order,
																							 $this->skip,
																							 $this->take);
		return count($data);
	}

	/**
	 * Fetching a list of results by the given data
	 * @access public
	 * @return array A list of IGetable s, depending on the attributes it will be:
	 *               Readables(views or specific selects) or Writeables
	 */
	public function Iter()
	{
		$result = array();

		// if table
		if ($this->table->GetTableMode())
		{
			// if no specific selects, than a full object
			if (empty($this->select))
			{
				$keys = $this->getConnection()->RawRequest(array($this->table->GetPrimaryKey()),
																									 $this->table->GetName(),
																									 $this->where,
																									 $this->order,
																									 $this->skip,
																									 $this->take);
				foreach ($keys as $id)
				{
					$obj = new Writable($this->getConnection(), $this->table->GetName());
					$obj->UseId($id[$this->table->GetPrimaryKey()]);
					$result[] = $obj;
				}
			}
			else // otherwise just get specific select
			{
				$keys = $this->getConnection()->RawRequest($this->select,
																									 $this->table->GetName(),
																									 $this->where,
																									 $this->order,
																									 $this->skip,
																									 $this->take);
				foreach ($keys as $id)
				{
					$result[] = new Readable($this->getConnection(), $this->table->GetName(), $id);
				}
			}
		}
		else // if view
		{
			$keys = $this->getConnection()->RawRequest(empty($this->select) ? array('*') : $this->select,
																								 $this->table->GetName(),
																								 $this->where,
																								 $this->order,
																								 $this->skip,
																								 $this->take);
			foreach ($keys as $id)
			{
				$result[] = new Readable($this->getConnection(), $this->table->GetName(), $id);
			}
		}

		return $result;
	}

	/**
	 * Formats each result data into a string by the given pattern
	 * @access public
	 * @param string $format The format string
	 * @return string The formatted data
	 */
	public function Format($format)
	{
		$concat = "";
		foreach ($this->Iter() as $row)
		{
			$concat .= $row->Format($format);
		}

		return $concat;
	}
};
