<?php

namespace JB\DB;

require_once(dirname(__FILE__)."/Database.php");

/**
 * Base class for all database connected objects
 * @abstract
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Helper
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
abstract class Object
{
	/**
	 * The database connection
	 * @access private
	 */
	private $connection;

	/**
	 * The constructor taking the database connection
	 * @access protected
	 * @param \JB\DB\Database $database The database connection
	 */
	protected function __construct(Database $database)
	{
		$this->connection = $database;
	}

	/**
	 * Getter for the database connection
	 * @access protected
	 * @return \JB\DB\Database The database connection
	 */
	protected function getConnection()
	{
		return $this->connection;
	}
};
