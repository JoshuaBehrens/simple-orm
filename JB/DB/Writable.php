<?php

namespace JB\DB;

require_once(dirname(__FILE__)."/Object.php");
require_once(dirname(__FILE__)."/IGetable.php");

/**
 * Class to hold read- and writable data of a table
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Connection
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class Writable extends Object implements IGetable
{
	/**
	 * The table definition for table
	 * @access private
	 */
	private $table;

	/**
	 * The cached data
	 * @access private
	 */
	private $data;

	/**
	 * The primary key to work on
	 * @access private
	 */
	private $id;

	/**
	 * Constructor of the writable initializing the database connection
	 * @access public
	 * @param \JB\DB\Database $database The database connection
	 * @param string          $table    The table or view name
	 */
	public function __construct(Database $database, $table)
	{
		parent::__construct($database);
		$this->table = $database->GetDefinition($table);
		$this->data = null;
	$this->id = null;
	}

	/**
	 * Gets the primary key value
	 * @access public
	 * @return scalar The primary key value
	 */
	public function GetId()
	{
		return $this->id;
	}

	/**
	 * Sets the primary key to use
	 * @access public
	 * @param scalar $id The value
	 * @return void
	 */
	public function UseId($id)
	{
		$this->id = $id;
		$this->data = null;
	}

	/**
	 * Request data if not already cachec
	 * @access private
	 * @return bool True, if the cached data has any value otherwise False
	 */
	private function requestData()
	{
		if (is_null($this->data) && !is_null($this->id))
		{
			$stmt = $this->getConnection()->RawRequest(array('*'),
																								 $this->table->GetName(),
																								 OP\Where\Eq($this->table->GetPrimaryKey(), $this->id),
																								 null);

			$this->data = count($stmt) == 0 ? null : $stmt[0];
		}

		return !is_null($this->data);
	}

	/**
	 * Checks whether there is cached data as there is an entry with the given primary key
	 * @access public
	 * @return bool True if there is a row to work on otherwise false
	 */
	public function Exists()
	{
		return $this->requestData();
	}

	/**
	 * Gets the value lying behind the column name
	 * @access public
	 * @param string $col The colum name
	 * @return scalar|null The value of the column
	 */
	public function Get($col)
	{
		if ($this->requestData() && array_key_exists($col, $this->data))
		{
			return $this->data[$col];
		}

		return null;
	}

	/**
	 * Sets the value lying behind the column name
	 * @access public
	 * @param string      $col   The colum name
	 * @param scalar|null $value The value to set
	 * @param bool        $isRaw If true the value will be treated as raw sql command otherwise it will be late binded
	 * @return bool True, if there was an update, otherwise false
	 */
	public function Set($col, $value, $isRaw = false)
	{
		if ($this->requestData() && array_key_exists($col, $this->data) && $col != $this->table->GetPrimaryKey())
		{
			$count = $this->getConnection()->RawRowCount($this->table->GetName(),
																									 array($isRaw ? OP\Set\Raw($col, $value) : OP\Set\Value($col, $value)),
																									 OP\Where\Eq($this->table->GetPrimaryKey(), $this->GetId()));

	  if ($count > 0)
	  {
	    $this->data[$col] = $value;
	    return true;
	  }
		}

		return false;
	}

	/**
	 * Formats the data into a string by the given pattern
	 * @access public
	 * @param string $format The format string
	 * @return string The formatted data
	 */
	public function Format($format)
	{
		if ($this->requestData())
		{
			$data = $this->data;
			return preg_replace_callback('#\{(?<name>[^}]+)\}#', function ($matches) use ($data)
				{
					$value = $this->Get($matches['name']);
					return is_null($value) ? $matches[0] : $value;
				}, $format);
		}
		return $format;
	}
};
