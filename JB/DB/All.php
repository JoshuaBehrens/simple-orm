<?php

require_once(dirname(__FILE__)."/Binds.php");
require_once(dirname(__FILE__)."/Database.php");
require_once(dirname(__FILE__)."/Object.php");
require_once(dirname(__FILE__)."/Operators/Order.php");
require_once(dirname(__FILE__)."/Operators/Where.php");
require_once(dirname(__FILE__)."/Operators/Set.php");
require_once(dirname(__FILE__)."/IGetable.php");
require_once(dirname(__FILE__)."/Readable.php");
require_once(dirname(__FILE__)."/Writable.php");
require_once(dirname(__FILE__)."/SQLGenerator.php");
require_once(dirname(__FILE__)."/Updatable.php");
require_once(dirname(__FILE__)."/Selectable.php");
