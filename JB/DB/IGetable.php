<?php

namespace JB\DB;

/**
 * A basic interface to be able to group Readables and Writeables
 */
interface IGetable
{
	/**
	 * The get function to override
	 * @abstract
	 * @access public
	 * @param string $name The column name
	 * @return scalar|null Any value that belongs to the cell
	 */
	public function Get($name);
};