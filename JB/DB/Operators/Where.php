<?php

namespace JB\DB\OP\Where;

require_once(dirname(__FILE__)."/Operator.php");

/**
 * Basic interface to identify a where operator
 */
interface IWhere {};

/**
 * Creates an equals operator
 * @param string $name  Columnname to compare
 * @param scalar $value Value to compare
 * @return \JB\DB\OP\Where\Equals operator instance
 */
function Eq($name, $value)
{
	return new Equals($name, $value);
}

/**
 * Creates a not-equals operator
 * @param string $name  Columnname to compare
 * @param scalar $value Value to compare
 * @return \JB\DB\OP\Where\LogicalNot operator instance
 */
function Neq($name, $value)
{
	return LogicalNot(new Equals($name, $value));
}

/**
 * Creates a greater than operator
 * @param string $name  Columnname to compare
 * @param scalar $value Value to compare
 * @return \JB\DB\OP\Where\GreaterThan operator instance
 */
function Gt($name, $value)
{
	return new GreaterThan($name, $value);
}

/**
 * Creates a greater equals operator
 * @param string $name  Columnname to compare
 * @param scalar $value Value to compare
 * @return \JB\DB\OP\Where\GreaterEquals operator instance
 */
function Ge($name, $value)
{
	return new GreaterEquals($name, $value);
}

/**
 * Creates a less than operator
 * @param string $name  Columnname to compare
 * @param scalar $value Value to compare
 * @return \JB\DB\OP\Where\LessThan operator instance
 */
function Lt($name, $value)
{
	return new LessThan($name, $value);
}

/**
 * Creates a less equals operator
 * @param string $name  Columnname to compare
 * @param scalar $value Value to compare
 * @return \JB\DB\OP\Where\LessEquals operator instance
 */
function Le($name, $value)
{
	return new LessEquals($name, $value);
}

/**
 * Creates a like operator
 * @param string $name  Columnname to compare
 * @param scalar $value Value to compare
 * @return \JB\DB\OP\Where\Like operator instance
 */
function Like($name, $value)
{
	return new Like($name, $value);
}

/**
 * Creates an unlike operator
 * @param string $name  Columnname to compare
 * @param scalar $value Value to compare
 * @return \JB\DB\OP\Where\LogicalNot operator instance
 */
function Unlike($name, $value)
{
	return LogicalNot(new Like($name, $value));
}

/**
 * Creates an is null operator
 * @param string $name Columnname to test against null
 * @return \JB\DB\OP\Where\IsNull operator instance
 */
function IsNull($name)
{
	return new IsNull($name);
}

/**
 * Creates an is not null operator
 * @param string $name Columnname to test against null
 * @return \JB\DB\OP\Where\LogicalNot operator instance
 */
function IsNotNull($name)
{
	return LogicalNot(new IsNull($name));
}

/**
 * Creates an in operator
 * @param string $name   Columnname to test
 * @param array  $values The value list
 * @return \JB\DB\OP\Where\In operator instance
 */
function In($name, $values)
{
	return new In($name, $values);
}

/**
 * Creates a not in operator
 * @param string $name   Columnname to test
 * @param array  $values The value list
 * @return \JB\DB\OP\Where\LogicalNot operator instance
 */
function NotIn($name, $values)
{
	return LogicalNot(new In($name, $values));
}

/**
 * Creates a not operator
 * @param \JB\DB\OP\Where\IWhere $any Any amount of other condition operators
 * @return \JB\DB\OP\Where\LogicalNot operator instance
 */
function LogicalNot(IWhere $any)
{
	return new LogicalNot($any);
}

/**
 * Creates an and operator
 * @param array $any Any amount of other condition operators
 * @return \JB\DB\OP\Where\LogicalAnd operator instance
 */
function LogicalAnd($any)
{
	return new LogicalAnd(func_get_args());
}

/**
 * Creates an or operator
 * @param array $any Any amount of other condition operators
 * @return \JB\DB\OP\Where\LogicalOr operator instance
 */
function LogicalOr($any)
{
	return new LogicalOr(func_get_args());
}

/**
 * Operator is null
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class IsNull extends \JB\DB\OP\Operator implements IWhere
{
	/**
	 * The constructor with the column name
	 * @access public
	 * @param string $name The column name
	 */
	public function __construct($name)
	{
		parent::__construct($name);
	}
};

/**
 * Operator in
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class In extends \JB\DB\OP\Operator implements IWhere
{
	/**
	 * The values
	 * @access private
	 */
	private $values;

	/**
	 * The constructor with the column name and values
	 * @access public
	 * @param string $name   The column name
	 * @param array  $values The values to compare with
	 */
	public function __construct($name, array $values)
	{
		parent::__construct($name);
		$this->values = $values;
	}

	/**
	 * Gets the values
	 * @return array the values
	 */
	public function GetValues()
	{
		return $this->values;
	}
};

/**
 * Base class for operators with a column name and a value to compare with
 * @abstract
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
abstract class Value extends \JB\DB\OP\Operator implements IWhere
{
	/**
	 * The value
	 * @access private
	 */
	private $value;

	/**
	 * The constructor with name and value
	 * @param string $name  Column name
	 * @param scalar $value Value to test against
	 * @todo throw error
	 */
	public function __construct($name, $value)
	{
		parent::__construct($name);

		if (is_scalar($value))
		{
			$this->value = $value;
		}
	}

	/**
	 * Gets the value
	 * @return scalar the value
	 */
	public function GetValue()
	{
		return $this->value;
	}
};

/**
 * Operator equals
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class Equals extends Value
{
	/**
	 * The constructor with name and value
	 * @param string $name  Column name
	 * @param scalar $value Value to test against
	 */
	public function __construct($name, $value)
	{
		parent::__construct($name, $value);
	}
};

/**
 * Operator greater than
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class GreaterThan extends Value
{
	/**
	 * The constructor with name and value
	 * @param string $name  Column name
	 * @param scalar $value Value to test against
	 */
	public function __construct($name, $value)
	{
		parent::__construct($name, $value);
	}
};

/**
 * Operator greater equals
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class GreaterEquals extends Value
{
	/**
	 * The constructor with name and value
	 * @param string $name  Column name
	 * @param scalar $value Value to test against
	 */
	public function __construct($name, $value)
	{
		parent::__construct($name, $value);
	}
};

/**
 * Operator less than
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class LessThan extends Value
{
	/**
	 * The constructor with name and value
	 * @param string $name  Column name
	 * @param scalar $value Value to test against
	 */
	public function __construct($name, $value)
	{
		parent::__construct($name, $value);
	}
};

/**
 * Operator less equals
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class LessEquals extends Value
{
	/**
	 * The constructor with name and value
	 * @param string $name  Column name
	 * @param scalar $value Value to test against
	 */
	public function __construct($name, $value)
	{
		parent::__construct($name, $value);
	}
};

/**
 * Operator like
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class Like extends Value
{
	/**
	 * The constructor with name and value
	 * @param string $name  Column name
	 * @param scalar $value Value to test against
	 */
	public function __construct($name, $value)
	{
		parent::__construct($name, $value);
	}
};

/**
 * Base class for logical operators
 * @abstract
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
abstract class Logical implements IWhere
{
	/**
	 * The operators to combine
	 * @access private
	 */
	private $ops = array();

	/**
	 * The constructor with all operators to combine
	 * @access public
	 * @param array $ops Operators to combine
	 */
	public function __construct(array $ops)
	{
		$this->addRange($ops);
	}

	/**
	 * Helper to recursive iter through arrays of operators
	 * @access private
	 * @param array $op Operator array
	 * @return void
	 */
	private function addRange(array $op)
	{
		foreach ($op as $o)
		{
			if (is_array($o))
			{
				$this->addRange($o);
			}
			elseif (!is_null($o))
			{
				$this->add($o);
			}
		}
	}

	/**
	 * Helper to add any operator to the list
	 * @access private
	 * @param \JB\DB\OP\Where\IWhere $op Operator to add
	 * @return void
	*/
	private function add(IWhere $op)
	{
		$this->ops[] = $op;
	}

	/**
	 * Gets the operators
	 * @return array The operators
	 */
	public function GetValues()
	{
		return $this->ops;
	}
};

/**
 * Logical operator not
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class LogicalNot extends \JB\DB\OP\Operator implements IWhere
{
	/**
	 * Operator to negate
	 * @access private
	 */
	private $operator;

	/**
	 * The constructor with all operators to combine
	 * @access public
	 * @param array $ops Operators to combine
	 */
	public function __construct(IWhere $ops)
	{
		parent::__construct('');
		$this->operator = $ops;
	}

	/**
	 * Gets the operator to negate
	 * @return \JB\DB\OP\Where\IWhere The operator to negate
	 */
	public function GetOperator()
	{
		return $this->operator;
	}
};

/**
 * Logical operator and
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class LogicalAnd extends Logical
{
	/**
	 * The constructor with all operators to combine
	 * @access public
	 * @param array $ops Operators to combine
	 */
	public function __construct(array $ops)
	{
		parent::__construct($ops);
	}
};

/**
 * Logical operator or
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class LogicalOr extends Logical
{
	/**
	 * The constructor with all operators to combine
	 * @access public
	 * @param array $ops Operators to combine
	 */
	public function __construct(array $ops)
	{
		parent::__construct($ops);
	}
};