<?php

namespace JB\DB\OP\Set;

require_once(dirname(__FILE__)."/Operator.php");

/**
 * Basic interface to identify a set operator
 */
interface ISet { };

/**
 * Creates an set value operator
 * @param string $name  Columnname to set
 * @param scalar $value Value to set
 * @return \JB\DB\OP\Set\Value operator instance
 */
function Value($name, $value)
{
	return new Value($name, $value);
}

/**
 * Creates an set raw operator
 * @param string $name Columnname to set
 * @param scalar $raw  Raw query set
 * @return \JB\DB\OP\Set\Raw operator instance
 */
function Raw($name, $raw)
{
	return new Raw($name, $raw);
}

/**
 * Creates an set null operator
 * @param string $name Columnname to set
 * @return \JB\DB\OP\Set\Null operator instance
 */
function Null($name)
{
	return new Null($name);
}

/**
 * Creates an set now operator
 * @param string $name Columnname to set
 * @return \JB\DB\OP\Set\Now operator instance
 */
function Now($name)
{
	return new Now($name);
}

/**
 * Operator set value
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class Value extends \JB\DB\OP\Operator implements ISet
{
	/**
	 * The value to set
	 * @access private
	 */
	private $value;

	/**
	 * Gets the value to set
	 * @access public
	 * @return scalar The value
	 */
	public function GetValue()
	{
		return $this->value;
	}

	/**
	 * The constructor taking the column name and the value
	 * @param string $name  The column name
	 * @param scalar $value The value to set
	 * @todo throw Error
	 */
	public function __construct($name, $value)
	{
		parent::__construct($name);
		if (is_scalar($value))
		{
			$this->value = $value;
		}
	}
};

/**
 * Operator set raw
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class Raw extends \JB\DB\OP\Operator implements ISet
{
	/**
	 * The value to set
	 * @access private
	 */
	private $value;

	/**
	 * Gets the value to set
	 * @access public
	 * @return string The value
	 */
	public function GetRaw()
	{
		return $this->value;
	}

	/**
	 * The constructor taking the column name and the value
	 * @param string $name The column name
	 * @param string $raw  The value to set
	 * @todo throw Error
	 */
	public function __construct($name, $raw)
	{
		parent::__construct($name);
		if (is_string($raw))
		{
			$this->$value = $raw;
		}
	}
};

/**
 * Operator set null
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class Null extends \JB\DB\OP\Operator implements ISet
{
	/**
	 * The constructor taking the column name
	 * @param string $name The column name
	 */
	public function __construct($name)
	{
		parent::__construct($name);
	}
};

/**
 * Operator set now
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class Now extends \JB\DB\OP\Operator implements ISet
{
	/**
	 * The constructor taking the column name
	 * @param string $name The column name
	 */
	public function __construct($name)
	{
		parent::__construct($name);
	}
};