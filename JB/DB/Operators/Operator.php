<?php

namespace JB\DB\OP;

/**
 * Base class for operators with a column name
 * @abstract
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
abstract class Operator
{
	/**
	 * The column name
	 * @access private
	 */
	private $name;

	/**
	 * The constructor with the column name
	 * @access public
	 * @param string $name The column name
	 * @todo throw error if $name is not a string
	 */
	public function __construct($name)
	{
		if (is_string($name))
		{
			$this->name = $name;
		}
	}

	/**
	 * Gets the column name
	 * @return string the column name
	 */
	public function GetName()
	{
		return $this->name;
	}
};
