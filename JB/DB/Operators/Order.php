<?php

namespace JB\DB\OP\Order;

require_once(dirname(__FILE__)."/Operator.php");

/**
 * Basic interface to identify a order operator
 */
interface IOrder { };

/**
 * Creates an ascending order operator
 * @param string $name Columnname to set
 * @return \JB\DB\OP\Order\Ascending operator instance
 */
function Asc($name)
{
	return new Ascending($name);
}

/**
 * Creates a descending order operator
 * @param string $name Columnname to set
 * @return \JB\DB\OP\Order\Descending operator instance
 */
function Desc($name)
{
	return new Descending($name);
}

/**
 * Operator ordering ascending
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class Ascending extends \JB\DB\OP\Operator implements IOrder
{
	/**
	 * The constructor taking the column name
	 * @access public
	 * @param string $name The column name
	 */
	public function __construct($name)
	{
		parent::__construct($name);
	}
};

/**
 * Operator ordering ascending
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Operator
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class Descending extends \JB\DB\OP\Operator implements IOrder
{
	/**
	 * The constructor taking the column name
	 * @access public
	 * @param string $name The column name
	 */
	public function __construct($name)
	{
		parent::__construct($name);
	}
};
