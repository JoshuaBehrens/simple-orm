<?php

namespace JB\DB\SQL;

require_once(dirname(__FILE__)."/../Operators/Where.php");
require_once(dirname(__FILE__)."/../Operators/Set.php");
require_once(dirname(__FILE__)."/../SQLGenerator.php");

class MySQL extends \JB\DB\SQLGenerator
{
	/**
	 * Generates from the given values a select statement and the used late-binds
	 * @access public
	 * @param array                       $select The columns to select
	 * @param string                      $from   The table or view name
	 * @param \JB\DB\OP\Where\IWhere|null $where  The where condition or null if no condition given
	 * @param array|null                  $order  The order to sort
	 * @param int                         $skip   The amount of items to skip
	 * @param int                         $take   The amount to return (-1 = all)
	 * @return array Array containing the sql statement and the late-binds
	 */
	public function Select(array $select, $from, $where, $order, $skip, $take)
	{
		$binds = new \JB\DB\Binds();
		$where = is_null($where) ? "" : $this->solveOperator($where, $binds);
		if (strlen(trim($where)) > 0)
		{
			$where = " WHERE $where";
		}

		$order = empty($order) ? "": $this->solveOrderOperators($order);
		if (strlen(trim($order)) > 0)
		{
			$order = " ORDER BY $order";
		}

		$selectClause = array();
		foreach ($select as $col)
		{
			if (!empty($col))
			{
				$selectClause[] = ($col != '*' && $col[strlen($col) - 1] == ']') ? "[$col]" : $col;
			}
		}

		$limit = "";
		if ($take > 0)
		{
			$limit = " LIMIT $take";
			if ($skip > 0)
			{
				$limit = "$limit OFFSET $skip";
			}
		}
		elseif ($skip > 0)
		{
			$limit = " LIMIT -1 OFFSET $skip";
		}

		return array("SELECT ".join(", ", $selectClause)." FROM `$from` $where $order $limit", $binds);
	}

	/**
	 * Generates from the given values an update statement and the used late-binds
	 * @access public
	 * @param string                      $table The table
	 * @param array                       $set   The columns to set
	 * @param \JB\DB\OP\Where\IWhere|null $where The where condition or null if no condition given
	 * @return array Array containing the sql statement and the late-binds
	 */
	public function Update($table, array $set, $where)
	{
		$binds = new \JB\DB\Binds();
		$where = is_null($where) ? "" : $this->solveOperator($where, $binds);
		if (strlen(trim($where)) > 0)
		{
			$where = " WHERE $where";
		}

		$command = array();
		foreach ($set as $subset)
		{
			$command[] = $this->solveSetOperatorAsUpdate($subset, $binds);
		}

		$setClause = join(", ", $command);

		if (strlen(trim($setClause)) > 0)
		{
			$setClause = " SET $setClause ";
		}

		return array("UPDATE `$table` $setClause $where", $binds);
	}

	/**
	 * Generates from the given values an insert statement and the used late-binds
	 * @access public
	 * @param string $table The table
	 * @param array  $set   The values to insert
	 * @return array Array containing the sql statement and the late-binds
	 */
	public function Insert($table, array $set)
	{
		$binds = new \JB\DB\Binds();

		$keys = array();
		$values = array();
		foreach ($set as $subset)
		{
			list($keys[], $values[]) = $this->solveSetOperatorAsInsert($subset, $binds);
		}

		return array("INSERT INTO `$table`(".join(", ", $keys).") VALUES(".join(", ", $values).")", $binds);
	}

	/**
	 * Gets a table definition from the given database and table/view
	 * @access public
	 * @param \JB\DB\Database $database The database connection
	 * @param string          $table    The table/view name
	 * @return \JB\DB\TableDefinition The table definition
	 */
	public function Definition(\JB\DB\Database $database, $table)
	{
		$binds = new \JB\DB\Binds();

		$queryString = "SELECT TABLE_TYPE FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ".$binds->Add($table);
		$stmt = $database->Statement($queryString, $binds);
		if (!is_null($stmt))
		{
			$result = new \JB\DB\TableDefinition($table);

			$tableType = $stmt->fetchAll();
			$tableType = array_shift($tableType);
			$tableType = array_shift($tableType);
			$result->SetTableMode($tableType == 'BASE TABLE');

			if (!is_null($stmt = $database->Statement("EXPLAIN `$table`", new \JB\DB\Binds())))
			{
				$defs = $stmt->fetchAll();

				if (count($defs) == 0)
				{
					throw new \InvalidArgumentException("Table '$table' does not exist.", 404);
				}

				foreach ($defs as $val)
				{
					$result->AddColumn(new \JB\DB\ColumnDefinition($val['Field'],
																												 $val['Type'],
																												 $val['Null'] == "YES",
																												 $val['Default']));

					if ($val["Key"] === 'PRI')
					{
						$result->SetPrimaryKey($val['Field']);
					}
				}

				return $result;
			}
		}

		return null;
	}

	/**
	 * Converts an array of Order operators into an sql clause
	 * @access private
	 * @param array $ops The array of Order operators
	 * @return string The generated sql clause
	 */
	private function solveOrderOperators(array $ops)
	{
		$result = array();

		foreach ($ops as $op)
		{
			$val = $this->solveOrderOperator($op);
			if (!empty($val))
			{
				$result[] = $val;
			}
		}

		return join(", ", $result);
	}

	/**
	 * Converts an Order operator into an sql clause
	 * @access private
	 * @param \JB\DB\OP\Order\IOrder $op The Order operator
	 * @return string The generated sql clause
	 */
	private function solveOrderOperator(\JB\DB\OP\Order\IOrder $op)
	{
		switch (get_class($op))
		{
			case 'JB\DB\OP\Order\Ascending':
				return "`{$op->GetName()}` ASC";
			case 'JB\DB\OP\Order\Descending':
				return "`{$op->GetName()}` DESC";
		}

		return null;
	}

	/**
	 * Converts a Set operator into an insert sql clause
	 * @access private
	 * @param \JB\DB\OP\Set\ISet $op    The Set operator
	 * @param \JD\DB\Binds       $binds The bind managing instance
	 * @return string The generated sql clause
	 */
	private function solveSetOperatorAsInsert(\JB\DB\OP\Set\ISet $op, \JB\DB\Binds $binds)
	{
		switch (get_class($op))
		{
			case 'JB\DB\OP\Set\Now':
				return array("`{$op->GetName()}`", "CURRENT_TIMESTAMP");
			case 'JB\DB\OP\Set\Null':
				return array("`{$op->GetName()}`", "NULL");
			case 'JB\DB\OP\Set\Raw':
				// Injection PLS
				return array("`{$op->GetName()}`", $op->GetValue());
			case 'JB\DB\OP\Set\Value':
				return array("`{$op->GetName()}`", $binds->Add($op->GetValue()));
		}

		return array("", "");
	}

	/**
	 * Converts a Set operator into a set sql clause
	 * @access private
	 * @param \JB\DB\OP\Set\ISet $op    The Set operator
	 * @param \JD\DB\Binds       $binds The bind managing instance
	 * @return string The generated sql clause
	 */
	private function solveSetOperatorAsUpdate(\JB\DB\OP\Set\ISet $op, \JB\DB\Binds $binds)
	{
		switch (get_class($op))
		{
			case 'JB\DB\OP\Set\Now':
				return "`{$op->GetName()}` = CURRENT_TIMESTAMP";
			case 'JB\DB\OP\Set\Null':
				return "`{$op->GetName()}` = NULL";
			case 'JB\DB\OP\Set\Raw':
				// Injection PLS
				return "`{$op->GetName()}` = ".$op->GetValue();
			case 'JB\DB\OP\Set\Value':
				return "`{$op->GetName()}` = ".$binds->Add($op->GetValue());
		}

		return "";
	}

	/**
	 * Converts a Logical operator into a where sql clause
	 * @access private
	 * @param \JB\DB\OP\Where\Logical $op    The logical Where operator
	 * @param \JD\DB\Binds            $binds The bind managing instance
	 * @return string The generated sql clause
	 */
	private function solveLogicalOperator(\JB\DB\OP\Where\Logical $op, \JB\DB\Binds $binds)
	{
		switch (get_class($op))
		{
		case 'JB\DB\OP\Where\LogicalAnd':
			{
				$count = 0;
				$ops = array();
				foreach ($op->GetValues() as $o)
				{
					$ops[] = $this->solveOperator($o, $binds);
					$count++;
				}

				return ($count > 0) ? "(".join(" AND ", $ops).")" : "";
			}
		case 'JB\DB\OP\Where\LogicalOr':
			{
				$count = 0;
				$ops = array();
				foreach ($op->GetValues() as $o)
				{
					$ops[] = $this->solveOperator($o, $binds);
					$count++;
				}

				return ($count > 0) ? "(".join(" OR ", $ops).")" : "";
			}
		case 'JB\DB\OP\Where\LogicalNot':
			return "NOT(".$this->solveOperator($op->GetOperator(), $binds).")";
		}

		return "";
	}

	/**
	 * Converts a Where operator into a where sql clause
	 * @access private
	 * @param \JB\DB\OP\Where\IWhere $op    The Where operator
	 * @param \JD\DB\Binds           $binds The bind managing instance
	 * @return string The generated sql clause
	 */
	private function solveOperator(\JB\DB\OP\Where\IWhere $op, \JB\DB\Binds $binds)
	{
		switch (get_class($op))
		{
		case 'JB\DB\OP\Where\Equals':
			return "`{$op->GetName()}` = ".$binds->Add($op->GetValue());
		case 'JB\DB\OP\Where\Like':
			return "`{$op->GetName()}` LIKE ".$binds->Add($op->GetValue());
		case 'JB\DB\OP\Where\GreaterEquals':
			return "`{$op->GetName()}` >= ".$binds->Add($op->GetValue());
		case 'JB\DB\OP\Where\LessEquals':
			return "`{$op->GetName()}` <= ".$binds->Add($op->GetValue());
		case 'JB\DB\OP\Where\GreaterThan':
			return "`{$op->GetName()}` > ".$binds->Add($op->GetValue());
		case 'JB\DB\OP\Where\LessThan':
			return "`{$op->GetName()}` < ".$binds->Add($op->GetValue());
		case 'JB\DB\OP\Where\In':
			{
				$ins = array();
				foreach ($op->GetValues() as $val)
				{
					$ins[] = $binds->Add($val);
				}
				return (count($ins) > 0) ? "`{$op->GetName()}` IN (".join(", ", $ins).")" : "";
			}
		case 'JB\DB\OP\Where\IsNull':
			return "`{$op->GetName()}` IS NULL";
		default:
			return $this->solveLogicalOperator($op, $binds);
		}

		return "";
	}
};