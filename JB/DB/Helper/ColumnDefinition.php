<?php

namespace JB\DB;

/**
 *
 */
class ColumnDefinition
{
	/**
	 * The name
	 * @access private
	 */
	private $name;

	/**
	 * The type
	 * @access private
	 */
	private $type;

	/**
	 * Is nullable
	 * @access private
	 */
	private $null;

	/**
	 * Default value
	 * @access private
	 */
	private $default;

	/**
	 * The constructor
	 * @access public
	 * @param string      $name     The column name
	 * @param string      $type     The sql type
	 * @param bool        $nullable True, if nullable otherwise false
	 * @param null|scalar $default  Default value
	 */
	public function __construct($name, $type, $nullable, $default)
	{
		$this->name = is_string($name) ? $name : null;
		$this->type = $type ? is_string($type) : null;
		$this->null = $nullable ? is_bool($nullable) : false;
		$this->default = $default;
	}

	/**
	 * Gets the columns name
	 * @return string The columns name
	 */
	public function GetName()
	{
		return $this->name;
	}

	/**
	 * Gets the columns type
	 * @return string The colums type
	 */
	public function GetType()
	{
		return $this->type;
	}

	/**
	 * Gets whether the type is nullable
	 * @return bool True, if nullable otherwise false
	 */
	public function IsNullable()
	{
		return $this->null;
	}

	/**
	 * Gets the default value
	 * @return null|scalar The default
	 */
	public function GetDefault()
	{
		return $this->default;
	}
};