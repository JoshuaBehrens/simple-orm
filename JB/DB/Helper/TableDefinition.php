<?php

namespace JB\DB;

require_once(dirname(__FILE__)."/ColumnDefinition.php");

/**
 *
 */
class TableDefinition
{
	private $isTable;
	private $primaryKey;
	private $uniqueKeys;
	private $columns;
	private $name;

	/**
	 * Constructor
	 * @access public
	 * @param string $name Table name
	 */
	public function __construct($name)
	{
		$this->columns = array();
		$this->isTable = true;
		$this->primaryKey = null;
		$this->uniqueKeys = array();
		$this->name = $name;
	}

	/**
	 * Gets the table name
	 * @access public
	 * @return string The table name
	 */
	public function GetName()
	{
		return $this->name;
	}

	/**
	 * Sets the definition to be either a table or a view
	 * @access public
	 * @param bool $value True for table, false for view
	 * @return void
	 */
	public function SetTableMode($value)
	{
		if (is_bool($value))
		{
			$this->isTable = $value;
		}
	}

	/**
	 * Gets the table mode
	 * @access public
	 * @return bool True, if table otherwise false
	 */
	public function GetTableMode()
	{
		return $this->isTable;
	}

	/**
	 * Sets the primary column
	 * @access public
	 * @param string $value The column name
	 * @return void
	 */
	public function SetPrimaryKey($value)
	{
		if (is_string($value) && array_key_exists($value, $this->columns) && !in_array($value, $this->uniqueKeys))
		{
			$this->primaryKey = $value;
		}
	}

	/**
	 * Gets the primary column name
	 * @access public
	 * @return string The primary column name
	 */
	public function GetPrimaryKey()
	{
		return $this->primaryKey;
	}

	/**
	 * Adds a column to be a unique column
	 * @access public
	 * @param string $value The column name
	 * @return void
	 */
	public function AddUniqueColumn($value)
	{
		if (is_string($value) &&
				array_key_exists($value, $this->columns) &&
				!in_array($value, $this->uniqueKeys) &&
				$this->primaryKey != $value)
		{
			$this->uniqueKeys[] = $value;
		}
	}

	/**
	 * Gets all unique column names
	 * @access public
	 * @return array Column names
	 */
	public function GetUniqueColumns()
	{
		return $this->uniqueKeys;
	}

	/**
	 * Adds a column definition
	 * @access public
	 * @param \JB\DB\ColumnDefinition $col The column definition
	 * @return void
	 */
	public function AddColumn(ColumnDefinition $col)
	{
		$this->columns[$col->GetName()] = $col;
	}

	/**
	 * Gets all column definitions
	 * @return array All registered columns
	 */
	public function GetColumns()
	{
		return $this->columns;
	}
};