<?php

namespace JB\DB;

/**
 * Class to manage a query's bindlist and bind naming
 * @access public
 * @author Joshua Behrens <author@joshua-behrens.de>
 * @category Helper
 * @copyright Copyright (c) 2015, Joshua Behrens
 */
class Binds implements \Iterator
{
	/**
	 * List of all binds
	 * @access private
	 */
	private $binds = array();

	/**
	 * List of all names given
	 * @access private
	 */
	private $keys = array();

	/**
	 * Iterator index to iter over the binds
	 * @access private
	 */
	private $index = -1;

	/**
	 * Add a value to the bind list
	 * @access public
	 * @param scalar $value The value to bind
	 * @return string The given name for the bound value
	 * @todo add throw exception if from content in $value or currently in iteration process
	 */
	public function Add($value)
	{
		if (!$this->valid() && is_scalar($value))
		{
			$name = count($this->binds);
			$this->binds["b$name"] = $value;
			$this->keys[] = "b$name";
			return ":b$name";
		}

		return null;
	}

	/**
	 * Returns the current bind list as array
	 * @access public
	 * @return array The current bind list
	 */
	public function AsArray()
	{
		return $this->binds;
	}

	/**
	 * Implementation for the Iterator interface
	 * @access public
	 * @return scalar current value of iteration
	 */
	public function current()
	{
		if ($this->valid())
		{
			return $this->binds[$this->keys[$this->index]];
		}

		return null;
	}

	/**
	 * Implementation for the Iterator interface
	 * @access public
	 * @return string current key of iteration
	 */
	public function key()
	{
		if ($this->valid())
		{
			return $this->keys[$this->index];
		}

		return null;
	}

	/**
	 * Implementation for the Iterator interface
	 * @access public
	 * @return void
	 */
	public function next()
	{
		$this->index++;
	}

	/**
	 * Implementation for the Iterator interface
	 * @access public
	 * @return void
	 */
	public function rewind()
	{
		$this->index = 0;
	}

	/**
	 * Implementation for the Iterator interface
	 * @access public
	 * @return bool True, if in a valid iteration step, otherwise False
	 */
	public function valid()
	{
		return -1 < $this->index && $this->index < count($this->keys);
	}
};